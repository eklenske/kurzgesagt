# kurzgesagt

This puzzle only works in combination with a painting at the University of
Tübingen. Find the painting of Carl Friedrich Gauß and you'll know what to do.